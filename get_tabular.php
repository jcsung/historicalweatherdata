#!/usr/bin/php
<?php

require 'parse.php';

$searchstr="temp.";

//0. Grab the date being passed in in MM DD YYYY format
$argc=$_SERVER['argc'];
$argv=$_SERVER['argv'];
if ($argc-1<3){
	die("Syntax: ".$argv[0]." MM DD YYYY\n");
}
$mm=$argv[1];
$dd=$argv[2];
$yyyy=$argv[3];

//1. Grab the website data from cur
$data=strtolower(file_get_contents("cur"));

//2. Get the tables from the site
$tables=read_tables($data);
//echo print_r($tables,true)."\n";

//3. Figure out the relevant table
$flag=-1;
for ($x=0; $x<sizeOf($tables); $x++){
	for ($y=0; $y<sizeOf($tables[$x]); $y++){
		for ($z=0; $z<sizeOf($tables[$x][$y]); $z++){
			if(strpos($tables[$x][$y][$z],$searchstr)!==false){
				$flag=$x; 
				break;
			}
		}
		if($flag>=0) break;
	}
	if ($flag>=0) break;
}

//echo "Relevant table index: $flag\n";

if ($flag<0){
	die("Error: No relevant table found.  Dumping table data:\n".print_r($tables,true)."\n");
}

$table=$tables[$flag]; //Okay, $table will be the relevant table

//echo print_r($tables[$flag],true)."\n";

/*
            [0] => time (pst)
            [1] => temp.
            [2] => dew point
            [3] => humidity
            [4] => pressure
            [5] => visibility
            [6] => wind dir
            [7] => wind speed
            [8] => gust speed
            [9] => precip
            [10] => events
            [11] => conditions

*/

//4. Now create a CSV

$keys=explode(",","time (pst),temp.,humidity");
$indx=explode(",","-1,-1,-1");

for ($x=0; $x<sizeOf($tables[$flag][0]); $x++){
	$cur=$tables[$flag][0][$x];
	for ($y=0; $y<sizeOf($keys); $y++){
		//echo "$cur\n";
		if ($keys[$y]==$cur){
			$indx[$y]=$x;
			break;
		}
	}
}

//echo print_r($tables[$flag][0],true)."\n";
//echo print_r($indx,true)."\n";

$fin="Historical.Weather.$yyyy.$mm.$dd.csv";
$outfile=fopen($fin,"w");
for ($x=1; $x<sizeOf($tables[$flag]); $x++){
	$cur=$tables[$flag][$x];
	
	//a.) Time
	$time=strtoupper($cur[$indx[0]]); 
	$time="$mm/$dd/$yyyy $time";
	//b.) Temperature
	$temp=$cur[$indx[1]];
	$temp=explode(" ",$temp);
	$temp=$temp[0];
	//c.) Humidity
	$humi=str_replace("%","",$cur[$indx[2]]);
	fwrite($outfile,"$time,$temp,$humi\n");
	//echo "$time,$temp,$humi\n";
}
fclose($outfile);

?>
