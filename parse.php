<?php
/*

	Table parsing functions
	Call parse($data), where $data is the full html page
	Note: $data must have been strtolower'd, so that <table>, <tr>, and 
              <td> tags can be removed
*/


function strip($string,$tag){
	//1. Make sure full tag being used
	if (preg_match("/<([a-z])+>/",$tag)===0){
		echo "Error: Invalid tag\n";
		return false;
	}
	//2. Figure out what the ending tag is
	$endtag=substr($tag,0,1)."/".substr($tag,1);

	//3. Drop the > from the starting tag 
	$start_needle=substr($tag,0,strlen($tag)-1);

	//4. Remove the tag start
	$result=array();
	$temp=explode($start_needle,$string);
	for($x=1; $x<sizeOf($temp); $x++){
		$temp2=explode($endtag,$temp[$x]);
		array_push($result,$temp2[0]);
	}

	//echo print_r($result,true)."\n***\n";

	//5. Remove the rest of the tag
	for ($x=0; $x<sizeOf($result); $x++){
		$str="";
		$flag=false;
		for ($y=0; $y<strlen($result[$x]); $y++){
			if($flag) $str.=$result[$x][$y];
			if(!$flag&&$result[$x][$y]==">") $flag=true;
		}
		$result[$x]=$str;
	}
	
	//echo print_r($result,true)."\n";

	//7. Return result
	return $result;	
}

function manual_html_entity_decode($data){
	//$data=str_replace("
	$data=str_replace("&nbsp;"," ",$data);
	$data=str_replace("&deg;f","°F",$data);
	return $data;
}

function read_tables($data){
//Precondition: $data has already been strtolower'd

	//1. Remove all the <table>-related tags
	$tables=array();
	$raw=strip($data,"<table>");
	for ($x=0; $x<sizeOf($raw); $x++){
		//$tables[$x]['raw']=$raw[$x];
		$tables[$x]=array();
		$raw_rows=strip($raw[$x],"<tr>");
		for ($y=0; $y<sizeOf($raw_rows); $y++){
			$tables[$x][$y]=array();//$raw_rows[$y];
			$raw_cols=strip($raw_rows[$y],"<td>");
			for ($z=0; $z<sizeOf($raw_cols); $z++){
				$tables[$x][$y][$z]=manual_html_entity_decode(trim($raw_cols[$z]));
			}
		}
	}

	//echo print_r($tables,true)."\n****\n";

	//2.Strip remaining HTML
	for ($x=0; $x<sizeOf($tables); $x++){
		for ($y=0; $y<sizeOf($tables[$x]); $y++){
			for ($z=0; $z<sizeOf($tables[$x][$y]); $z++){
				$raw=$tables[$x][$y][$z];
				//Need to remove everything between < and >
				$str="";
				$flag=true;
				for ($i=0; $i<strlen($raw); $i++){
					if(substr($raw,$i,1)=="<") $flag=false;
					else if (substr($raw,$i,1)==">") $flag=true;
					if($flag&&substr($raw,$i,1)!=">") $str.=substr($raw,$i,1);
				}
				$tables[$x][$y][$z]=$str;
			}
		}
	}	

	//echo print_r($tables,true)."\n";
	return $tables;
}

?>
